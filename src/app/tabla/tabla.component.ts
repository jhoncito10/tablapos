import { Component, OnInit } from '@angular/core';

declare var $:any;

@Component({
  selector: 'app-tabla',
  templateUrl: './tabla.component.html',
  styleUrls: ['./tabla.component.css']
})
export class TablaComponent implements OnInit {

  tabla = [
            {seleccion:'Colombia',icon:'src/app/tabla/images/colombia.png',partidos:18,puntos:26},
            {seleccion:'Brasil',icon:'../images/brasil.png',partidos:18,puntos:38},
            {seleccion:'Argentina',icon:'../images/argentina.png',partidos:18,puntos:25},
            {seleccion:'Ecuador',icon:'../images/ecuador.png',partidos:18,puntos:20},
            {seleccion:'Venezuela',icon:'../images/venezuela.png',partidos:18,puntos:9},
            {seleccion:'Uruguay',icon:'../images/uruguay.png',partidos:18,puntos:28},
            {seleccion:'Paraguay',icon:'../images/paraguay.png',partidos:18,puntos:24},
            {seleccion:'Peru',icon:'../images/peru.png',partidos:18,puntos:25},
            {seleccion:'Bolivia',icon:'../images/bolivia.png',partidos:18,puntos:14},
            {seleccion:'Chile',icon:'../images/chile.png',partidos:18,puntos:26},
  ];

  tabla2 = [
    {seleccion:'Colombia',icon:'tabla\images\colombia.png',partidos:18,puntos:26},
    {seleccion:'Brasil',icon:'../images/brasil.png',partidos:18,puntos:38},
    {seleccion:'Argentina',icon:'../images/argentina.png',partidos:18,puntos:25},
    {seleccion:'Ecuador',icon:'../images/ecuador.png',partidos:18,puntos:20},
    {seleccion:'Venezuela',icon:'../images/venezuela.png',partidos:18,puntos:9},
    {seleccion:'Uruguay',icon:'../images/uruguay.png',partidos:18,puntos:28},
    {seleccion:'Paraguay',icon:'../images/paraguay.png',partidos:18,puntos:24},
    {seleccion:'Peru',icon:'../images/peru.png',partidos:18,puntos:25},
    {seleccion:'Bolivia',icon:'../images/bolivia.png',partidos:18,puntos:14},
    {seleccion:'Chile',icon:'../images/chile.png',partidos:18,puntos:26},
  ];
  constructor() { 

    this.ordena2();
    console.log(this.tabla);
  }

  ngOnInit() {
    $('#checkPar').click(function(){$('#checkEPV').prop('checked',false);$('#checkVen').prop('checked',false);});
    $('#checkEPV').click(function(){$('#checkPar').prop('checked',false);$('#checkVen').prop('checked',false);});
    $('#checkVen').click(function(){$('#checkEPV').prop('checked',false);$('#checkPar').prop('checked',false);});

    $('#checkBra').click(function(){$('#checkEBraCo').prop('checked',false);$('#checkCo').prop('checked',false);});
    $('#checkEBraCo').click(function(){$('#checkBra').prop('checked',false);$('#checkCo').prop('checked',false);});
    $('#checkCo').click(function(){$('#checkEBraCo').prop('checked',false);$('#checkBra').prop('checked',false);});

    $('#checkEcu').click(function(){$('#checkEA').prop('checked',false);$('#checkArg').prop('checked',false);});
    $('#checkEA').click(function(){$('#checkEcu').prop('checked',false);$('#checkArg').prop('checked',false);});
    $('#checkArg').click(function(){$('#checkEA').prop('checked',false);$('#checkEcu').prop('checked',false);});

    $('#checkU').click(function(){$('#checkUB').prop('checked',false);$('#checkBo').prop('checked',false);});
    $('#checkUB').click(function(){$('#checkU').prop('checked',false);$('#checkBo').prop('checked',false);});
    $('#checkBo').click(function(){$('#checkUB').prop('checked',false);$('#checkU').prop('checked',false);});

    $('#checkPer').click(function(){$('#checkEPC').prop('checked',false);$('#checkCo').prop('checked',false);});
    $('#checkEPC').click(function(){$('#checkPer').prop('checked',false);$('#checkCo').prop('checked',false);});
    $('#checkCo').click(function(){$('#checkEPC').prop('checked',false);$('#checkPer').prop('checked',false);});

  }

  cambiaTablaGanador(Ganador,Perdedor){

        for (var i = 0; i < this.tabla.length; i++) {
          if(this.tabla[i].seleccion==Ganador){
            var dato = this.tabla[i].puntos;
            for (var j = 0; j < this.tabla2.length; j++) {
              if(this.tabla2[j].seleccion==Ganador){
                this.tabla2[j].puntos = dato;
                this.tabla2[j].puntos += 3;
              }
            }
          }
          if(this.tabla[i].seleccion==Perdedor){
            var dato2 = this.tabla[i].puntos;
            for (var k = 0; k < this.tabla2.length; k++) {
              if(this.tabla2[k].seleccion==Perdedor){
                this.tabla2[k].puntos = dato2;
              }
            }
          }

      }
    this.ordena2();
  }
  cambiaTablaEmpate(equipo1,equipo2){

            for (var i = 0; i < this.tabla.length; i++) {
              if(this.tabla[i].seleccion==equipo1){
                for (var j = 0; j < this.tabla2.length; j++) {
                  if(this.tabla2[j].seleccion==equipo1){
                    var dato = this.tabla[i].puntos;
                    this.tabla2[j].puntos = dato;
                    this.tabla2[j].puntos += 1;
                  }
                }
              }
              
              if(this.tabla[i].seleccion==equipo2){
                for (var k = 0; k < this.tabla2.length; k++) {
                  if(this.tabla2[k].seleccion==equipo2){
                    var dato2 = this.tabla[i].puntos;
                    this.tabla2[k].puntos = dato2;
                    this.tabla2[k].puntos += 1;
                  }
                }
              }
          }
    
          this.ordena2();

  }

  ordena2(){
    this.tabla2 = this.tabla2.sort(function OrdenarPorIdDescendente(x,y) {
      return y.puntos - x.puntos;
    });
  }

}
